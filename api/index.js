const Koa = require('koa')

// Create express instnace
const app = new Koa()

// Require API routes
const router = require('./routes/user')

// Import API Routes
app.use(router.routes())
app.use(router.allowedMethods())

// Export the server middleware
module.exports = {
  path: '/api',
  handler: app
}