const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();
const router = new Router();

// Mock Users
const users = [{
    name: 'Alexandre'
  },
  {
    name: 'Pooya'
  },
  {
    name: 'Sébastien'
  }
]

/* GET users listing. */
router.get('/users', (ctx, next) => {
  res.json(users)
})

/* GET user by ID. */
router.get('/users/:id', (ctx, next) => {
  const id = parseInt(req.params.id)
  if (id >= 0 && id < users.length) {
    res.json(users[id])
  } else {
    res.sendStatus(404)
  }
})

module.exports = router