import {
  fromJS
} from 'immutable';
import _ from 'lodash';
import Vuex from 'vuex';
import fetchDamaged from '../middleware/damagedLampFetcher';
import axios from 'axios'

const createStore = () => {
  return new Vuex.Store({
    state: {
      damaged: [],
      allInfo: [],
      tickets: [{
        name: "test"
      }],
      headers: ["Test header"],
      excelData: [],
      repairment: [],
      auth: null
    },
    mutations: {
      SET_DAMAGED: set('damaged'),
      SET_ALLINFO: set('allInfo'),
      SET_EXCELDATA: set('excelData'),
      DELETE_EXCELDATA: remove('excelData'),
      SET_REPAIRMENT: set('repairment'),
      SET_AUTH: set('auth')
    },
    actions: {
      init({
        commit
      }) {
        let user = null;
        if (process.browser) {
          user = JSON.parse(localStorage.getItem('auth'));
        }
        commit('SET_AUTH', user);
      },
      async login({
        commit
      }, user) {
        
        if (user.username == 'hki') {
          if (user.password == 'jwe@hki') {
            alert('login successfully');
            commit('SET_AUTH', user);
            this.$router.push('viewLamp')
          } else {
            alert('wrong password');
          }
        } else if (user.username == 'ntw') {
          if (user.password == 'jwe.ntw') {
            alert('login successfully');
            commit('SET_AUTH', user);
            this.$router.push('viewLamp')
          } else {
            alert('wrong password');
          }
        } else {
          alert('user not exist');
        }
        if (window.localStorage) {
          localStorage.setItem('auth', JSON.stringify(user));
        }
        
      },
      async setDamaged({
        commit
      }) {
        const url = 'http://192.168.10.231:5000/http://192.168.10.231:46080/';
        const opt = {
          method: 'GET',
          headers: {
            'X-Requested-With': 'XMLHttpRequest'
          },
          url,
        }

        const data = await axios(url);
        commit('SET_DAMAGED', data.data);
      },
      setAllInfo({
        commit
      }) {
        let allDamaged = this.state.damaged;
        for (let i = 0; i < allDamaged.length; i++) {
          allDamaged[i].location = 'location';
          allDamaged[i].district = 'district';
          allDamaged[i].source = 'source';
          allDamaged[i].orderDate = 'orderDate';
          allDamaged[i].repairDate = null;
          allDamaged[i].repairStaff = null;
          allDamaged[i].repaired = false;
        }
        commit('SET_ALLINFO', allDamaged);
      },
      updateInfo({
        commit
      }, newInfo) {
        commit('SET_ALLINFO', newInfo);
      },
      setExcelData({
        commit
      }, excelData) {
        commit('SET_EXCELDATA', excelData);
      },
      deleteExcelData({
        commit
      }, deleteItems) {
        commit('DELETE_EXCELDATA', deleteItems);
      },

      async setRepairment({
        commit
        }) {
          let url = '';
          if (this.state.auth) {
            switch (this.state.auth.username){
              case 'hki':
                url = 'http://192.168.10.231:5000/http://192.168.10.231:46080/repairment/hki';
                break;
              case 'ntw':
                url = 'http://192.168.10.231:5000/http://192.168.10.231:46080/repairment/ntw';
                break;
              default:
                alert('please login again');
                this.$router.push('index')
            }
            const opt = {
              method: 'GET',
              headers: {
                'X-Requested-With': 'XMLHttpRequest'
              },
              url,
            }

            const data = await axios(opt);
            for (let i = 0; i < data.data.length; i++) {
              Object.assign(data.data[i], {
                value: false,
                name: data.data[i].id
              });
            }

            commit('SET_REPAIRMENT', data.data);
          }
        //const url = 'http://59.148.251.104:34709/repairment';
      }
    }
  })
}

const set = key => (state, val) => {
  state[key] = val
}

const remove = key => (state, val) => {
  for (let x in val) {
    for (let y in state[key]) {
      let item = state[key][y].name;
      let target = val[x];
      if (item === target) {
        state[key].splice(y, 1);
        break;
      }
    }

  }
}

export default createStore;
