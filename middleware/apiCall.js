export const sendRequest = async (url, method, body, authorizationKey = null) => {
  let request = {
    headers: {
      //'authorization': authorizationKey ? authorizationKey : localStorage.getItem('authorizationKey'),
      'content-type': 'application/json',
      'Access-Control-Allow-Origin': 'http: //192.168.10.202:8070',
      //'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      //'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
    },
    method: method, // *GET, POST, PUT, DELETE, etc.
  };
  if (method == 'POST' || method == 'PUT') {
    request.body = body;
  }
  console.log(request);
  const response = await fetch(url, request);
  console.log(response);
  if (response.status >= 400) {
    throw new Error('bad request');
  }
  let {
    data,
    token
  } = await response.json();
  if (data) {
    console.log(data);
    return data;
  } else if (token) {
    console.log(token);
    return token;
  }
};
